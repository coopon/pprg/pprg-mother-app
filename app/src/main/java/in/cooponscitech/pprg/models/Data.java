package in.cooponscitech.pprg.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("video_host")
    @Expose
    private String videoHost;
    @SerializedName("image_host")
    @Expose
    private String imageHost;
    @SerializedName("data")
    @Expose
    private List<Video> videoData;

    public String getVideoHost() {
        return videoHost;
    }

    public void setVideoHost(String videoHost) {
        this.videoHost = videoHost;
    }

    public String getImageHost() {
        return imageHost;
    }

    public void setImageHost(String imageHost) {
        this.imageHost = imageHost;
    }

    public List<Video> getVideoData() {
        return videoData;
    }

    public void setVideoData(List<Video> videoData) {
        this.videoData = videoData;
    }
}
