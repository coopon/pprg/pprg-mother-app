package in.cooponscitech.pprg.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.cooponscitech.pprg.R;
import in.cooponscitech.pprg.activities.VideoPlayerActivity;
import in.cooponscitech.pprg.models.Video;
import in.cooponscitech.pprg.utils.GeneralUtils;
import in.cooponscitech.pprg.utils.SharedPref;
import in.cooponscitech.pprg.utils.ToastUtils;

public class VideoPlaylistAdapter extends RecyclerView.Adapter<VideoPlaylistAdapter.ViewHolder> {

    private List<Video> mList = new ArrayList<>();
    private Context mContext;
    private SharedPref pref;

    public VideoPlaylistAdapter(Context context) {
        this.mContext = context;
        this.pref = new SharedPref(mContext);
    }

    public void setVideos(List<Video> videoList) {
        this.mList = videoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_videos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Video video = mList.get(holder.getAdapterPosition());

        Glide.with(mContext)
                .load(pref.getImageHost() + video.getThumbnailUrl())
                .placeholder(R.drawable.thumbnail_place_holder)
                .into(holder.imgThumbnail);

        holder.txtTitle.setText(pref.isTamil() && !TextUtils.isEmpty(video.getTaTitle()) ? video.getTaTitle() : video.getTitle());
        holder.txtDescription.setText(pref.isTamil() && !TextUtils.isEmpty(video.getTaDescription()) ? video.getTaDescription() : video.getDescription());

        String duration = video.getDuration();

        /*if (video.getDuration() != null) {
            if (video.getDuration().contains(":")) {
                try {
                    int min = Integer.parseInt(video.getDuration().split(":")[0]);
                    int sec = Integer.parseInt(video.getDuration().split(":")[1]);
                    if (min > 0)
                        duration = min + "m ";
                    duration = duration + sec + "secs";
                } catch (Exception e) {
                    e.printStackTrace();
                    duration = video.getDuration();
                }
            } else
                duration = video.getDuration();
        }

        String info = duration + " ~ " + (video.getSize() != null ? video.getSize() : "");*/
        holder.txtInfo.setText(duration);

        holder.itemView.setOnClickListener(v -> openVideoPlayerActivity(video.getVideoUrl(), holder.txtTitle.getText().toString()));
    }

    private void openVideoPlayerActivity(String videoUrl, String title) {
        if (GeneralUtils.isNetworkConnected(mContext)) {
            Intent outgoing = new Intent(mContext, VideoPlayerActivity.class);
            outgoing.putExtra("VIDEO_URL", pref.getVideoHost() + videoUrl);
            outgoing.putExtra("VIDEO_TITLE", title);
            mContext.startActivity(outgoing);
        } else
            ToastUtils.showWarning(mContext, mContext.getString(R.string.msg_check_network_connection));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgThumbnail;
        TextView txtTitle;
        TextView txtDescription;
        TextView txtInfo;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgThumbnail = itemView.findViewById(R.id.img_thumbnail);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtDescription = itemView.findViewById(R.id.txt_description);
            txtInfo = itemView.findViewById(R.id.txt_info);
        }
    }
}
