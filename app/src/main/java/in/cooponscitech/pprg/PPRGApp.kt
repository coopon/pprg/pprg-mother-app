package `in`.cooponscitech.pprg

import `in`.cooponscitech.pprg.utils.SharedPref
import android.app.Application
import com.manimaran.crash_reporter.CrashReporter
import com.manimaran.crash_reporter.CrashReporterConfiguration

class PPRGApp : Application() {
    override fun onCreate() {
        super.onCreate()

        try {
            val emailIds = arrayOf("cooponscitech@gmail.com", "manimaraninam1027@gmail.com")
            val config = CrashReporterConfiguration()
                    .setMaxNumberOfCrashToBeReport(15)
                    .setAlertDialogPositiveButton("Send")
                    .setAlertDialogNegativeButton("Cancel")
                    .setIncludeDeviceInformation(true)
                    .setCrashReportSubjectForEmail("Crash Report For " + getString(R.string.app_name) + "App")
                    .setCrashReportSendEmailIds(emailIds)

            CrashReporter.initialize(applicationContext, config)
        }catch (e :Exception){
            e.printStackTrace()
        }
    }
}