package in.cooponscitech.pprg.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Arrays;

import in.cooponscitech.pprg.R;
import in.cooponscitech.pprg.fragments.ContentsViewFragment;
import in.cooponscitech.pprg.utils.GeneralUtils;
import in.cooponscitech.pprg.utils.SharedPref;
import in.cooponscitech.pprg.utils.ToastUtils;

public class MothersMainActivity extends AppCompatActivity {

    ContentsViewFragment contentsViewFragment;
    SharedPref pref;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        contentsViewFragment = new ContentsViewFragment();
        ft.replace(android.R.id.content, contentsViewFragment, "myFragmentTag");
        ft.commit();
        pref = new SharedPref(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options_mother, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_language:
                openLanguageSelection();
                return true;
            case R.id.menu_about_us:
                GeneralUtils.aboutUsActivity(this);
                return true;
            case R.id.menu_share:
                GeneralUtils.appShare(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openLanguageSelection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.select_language);
        String[] languageList = getResources().getStringArray(R.array.language);
        int checkedItem = Arrays.asList(languageList).indexOf(pref.getLanguage());
        builder.setSingleChoiceItems(languageList, checkedItem, (dialog, which) -> {
            pref.setLanguage(languageList[which]);
            if (contentsViewFragment != null)
                contentsViewFragment.changeLanguage();
            dialog.dismiss();
            invalidateOptionsMenu();
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setupLanguageSelectorMenuItem(Menu menu) {
        menu.findItem(R.id.menu_language).setTitle("Language\n(" + pref.getLanguage() + ")");
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        setupLanguageSelectorMenuItem(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce)
            super.onBackPressed();
        else {
            this.doubleBackToExitPressedOnce = true;
            ToastUtils.showWarning(getApplicationContext(), getString(R.string.msg_to_exit));
        }
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }
}