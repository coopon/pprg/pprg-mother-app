package in.cooponscitech.pprg.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.manimaran.crash_reporter.CrashReporter;
import com.manimaran.crash_reporter.interfaces.CrashAlertClickListener;
import com.manimaran.crash_reporter.utils.CrashUtil;

import in.cooponscitech.pprg.R;

public class SplashScreenActivity extends AppCompatActivity {

    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*
         not using a layout file to display splash screen.
         instead check res/drawable/splash_background.xml
         */

        /*
         following handler will close this splash screen
         after delayMillis and starts the welcome screen.
         */
        new Handler().postDelayed(() -> {
            try {
                CrashAlertClickListener listener = new CrashAlertClickListener() {
                    @Override
                    public void onOkClick() {
                        btnStart = findViewById(R.id.btnStart);
                        btnStart.setVisibility(View.VISIBLE);
                        btnStart.setOnClickListener(v -> {
                            callNextScreen();
                        });
                    }

                    @Override
                    public void onCancelClick() {
                        callNextScreen();
                    }
                };
                if (CrashUtil.Companion.isHaveCrashData()) {
                    CrashReporter.INSTANCE.showAlertDialogForShareCrash(this, listener, true);
                } else
                    callNextScreen();
            } catch (Exception e) {
                e.printStackTrace();
                callNextScreen();
            }
        }, 1000);
    }

    private void callNextScreen() {
        Intent intent = new Intent(getApplicationContext(), MothersMainActivity.class);
        startActivity(intent);
        finish();
    }
}