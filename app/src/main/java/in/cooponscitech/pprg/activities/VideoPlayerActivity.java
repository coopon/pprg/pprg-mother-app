package in.cooponscitech.pprg.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import in.cooponscitech.pprg.R;
import in.cooponscitech.pprg.utils.GeneralUtils;
import in.cooponscitech.pprg.utils.ToastUtils;

public class VideoPlayerActivity extends AppCompatActivity {

    PlayerView playerView;
    ExoPlayer videoPlayer;
    DefaultDataSourceFactory dataSourceFactory;
    private TextView txtVideoTitle;
    private ProgressBar progressBar;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra("VIDEO_URL")) {
            Uri videoUri = Uri.parse(intent.getStringExtra("VIDEO_URL"));
            txtVideoTitle = findViewById(R.id.txtVideoTitle);
            playerView = findViewById(R.id.playerView);
            progressBar = findViewById(R.id.progressBar);

            txtVideoTitle.setText(intent.getStringExtra("VIDEO_TITLE"));

            if (videoPlayer == null) {
                videoPlayer = new SimpleExoPlayer.Builder(this).build();

                videoPlayer.addListener(new Player.EventListener() {
                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        if (playbackState == Player.STATE_BUFFERING) {
                            progressBar.setVisibility(View.VISIBLE);
                        } else {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {
                        error.printStackTrace();
                        try {
                            if (GeneralUtils.isNetworkConnected(getApplicationContext())) {
                                ToastUtils.showError(getApplicationContext(), getString(R.string.something_went_wrong));
                            } else
                                ToastUtils.showWarning(getApplicationContext(), getString(R.string.msg_check_network_connection));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer2example"));
                final MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(videoUri);
                videoPlayer.prepare(videoSource);
                playerView.setPlayer(videoPlayer);
                videoPlayer.setPlayWhenReady(true);

                showTitle();
                playerView.setOnTouchListener((v, event) -> {
                    if (v != null && event.getAction() == MotionEvent.ACTION_UP)
                        showTitle();
                    return false;
                });
            }
        }
    }

    private void showTitle() {
        try {
            if (txtVideoTitle.getVisibility() == View.INVISIBLE) {
                txtVideoTitle.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    if (!isDestroyed() && !isFinishing())
                        txtVideoTitle.setVisibility(View.INVISIBLE);
                }, 2000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (playerView != null)
            videoPlayer.release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (playerView != null)
            videoPlayer.release();
    }
}
