package in.cooponscitech.pprg.rest;

import java.util.List;

import in.cooponscitech.pprg.models.Data;
import in.cooponscitech.pprg.models.Video;
import retrofit2.Call;
import retrofit2.http.GET;

public interface PPRGInterface {
    @GET("https://gitlab.com/coopon/pprg/pprg-web/-/raw/master/resources/data/content/pprg_base_data.json")
    Call<Data> getData();
}
