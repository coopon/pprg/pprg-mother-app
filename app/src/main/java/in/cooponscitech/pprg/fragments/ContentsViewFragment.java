package in.cooponscitech.pprg.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import in.cooponscitech.pprg.R;
import in.cooponscitech.pprg.adapters.VideoPlaylistAdapter;
import in.cooponscitech.pprg.models.Data;
import in.cooponscitech.pprg.models.Video;
import in.cooponscitech.pprg.rest.ApiClient;
import in.cooponscitech.pprg.rest.PPRGInterface;
import in.cooponscitech.pprg.utils.SharedPref;
import in.cooponscitech.pprg.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContentsViewFragment} factory method to
 * create an instance of this fragment.
 */
public class ContentsViewFragment extends Fragment {

    private SmoothProgressBar progressBar;
    private List<Video> videoList = new ArrayList<>();
    private VideoPlaylistAdapter adapter;


    public ContentsViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contents_view, container, false);


        // Views
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view_videos);
        SearchView searchView = rootView.findViewById(R.id.search_view);
        ImageView btnRefresh = rootView.findViewById(R.id.btnRefresh);
        progressBar = rootView.findViewById(R.id.smooth_progress_bar);

        adapter = new VideoPlaylistAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        /*
         * Search View
         */
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(getString(R.string.hint_search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                List<Video> filterList = new ArrayList<>();
                for (Video video : videoList) {
                    String rec = video.getTitle() + " " + video.getDescription() + " " + video.getTaTitle() + " " + video.getDescription();
                    rec = rec.replace("null", "").toLowerCase();
                    if (rec.contains(newText.toLowerCase())) {
                        filterList.add(video);
                    }
                }

                adapter.setVideos(filterList);
                adapter.notifyDataSetChanged();

                return false;
            }
        });

        getVideoPlayList();

        btnRefresh.setOnClickListener(v -> {
            getVideoPlayList();
        });


        return rootView;
    }

    private void getVideoPlayList() {

        Retrofit retrofit = ApiClient.getRetrofit();
        PPRGInterface pprgInterface = retrofit.create(PPRGInterface.class);

        videoList = new ArrayList<>();
        adapter.setVideos(videoList);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        pprgInterface.getData().enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                if (isAdded() && getContext() != null) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful() && response.body() != null) {
                        SharedPref sharedPref = new SharedPref(getContext());
                        sharedPref.setHostData(response.body().getImageHost(), response.body().getVideoHost());
                        videoList.addAll(response.body().getVideoData());
                        adapter.setVideos(videoList);
                        adapter.notifyDataSetChanged();
                    } else {
                        ToastUtils.showError(getContext(), getString(R.string.msg_check_network_connection));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (isAdded()) {
                    progressBar.setVisibility(View.INVISIBLE);
                    ToastUtils.showError(getContext(), getString(R.string.msg_check_network_connection));
                }
            }
        });

    }

    public void changeLanguage() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }
}
