package in.cooponscitech.pprg.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import coopon.manimaran.about_us_activity.AboutActivityBuilder;
import in.cooponscitech.pprg.BuildConfig;
import in.cooponscitech.pprg.R;


public class GeneralUtils {

    static String imgHost = "https://gitlab.com/coopon/pprg/pprg-web/-/raw/master/resources/data/content/thumbnails/";
    static String videoHost = "https://gitlab.com/coopon/pprg/pprg-web/-/raw/master/resources/data/content/videos/";

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        return nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
    }

    public static void aboutUsActivity(Context context) {

        // Base
        int appTheme = R.style.AppTheme;
        String aboutUsActivityTitle = "About Us";

        // App Details
        int appLogo = R.drawable.logo;
        String appName = context.getString(R.string.app_name);
        String appAbout = "பிரசவத்திற்குப் பிறகு தன்னையும் குழந்தையையும் கவனித்துக் கொள்ள தாய்க்கு இந்த மொபைல் செயலி உதவுகிறது. இதில் தாய் மற்றும் குழந்தைக்கான சுகாதார சேவை காணொளி மூலம் வழங்கப்படுகிறது.";
        String appVersion =  BuildConfig.VERSION_NAME +"_" + BuildConfig.VERSION_CODE;
        String appPlayStoreLink = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;


        // License details
        String licenseName = "GPL v3";
        String licenseUrl = "https://www.gnu.org/licenses/gpl-3.0.txt";

        // Share Details
        String shareMsgText = appName + " - Android App" + "\n\n" + appAbout + "\n\nApp Link :\n" + appPlayStoreLink + "\n\nShare with your friends";
        String shareIntentTitle = "Choose app to share...";

        // Powered By
        int poweredByIcon = R.drawable.coopon;
        String poweredByTitle = "Powered By";
        String poweredByName = "Coopon";
        String poweredByAbout = "Coopon is a (science & technology) worker's cooperative seeking to find economic sustainability through the use of Free S/W, H/W, & Design tools, skills, business models in the context of commons based peer production. ";
        String poweredByLink = "http://cooponscitech.in/";

        // Initiated By
        int initiatedByIcon = 0;//R.drawable.ic_initiator;
        String initiatedByTitle = "Initiated By";
        String initiatedByName = "Dr. Bala & Dr. Jayalakshmi";
        String initiatedByAbout = "Dr. Bala - Student of JIPMER, \nGuided By :\nDr. Jayalakshmi - JIPMER Social and Preventive Health Care Medicine department";
        String initiatedByLink = "balachandiran2012@gmail.com";

        // Others
        String sourceCodeLink = "https://gitlab.com/coopon/pprg/jipmer/-/tree/mother_app/android";
        int jsonResOfThirdPartyLibrary = R.raw.third_party_library;
        int jsonResOfCredits = R.raw.credits;
        String contactMail = "cooponscitech@gmail.com";


        /*
         *  All the values are set in about activity builder
         *  If we don't need any views just ignore in build.
         */
        new AboutActivityBuilder.Builder(context)
                .setTitle(aboutUsActivityTitle)
                .setAppLogo(appLogo)
                .setAppName(appName)
                .setAppAbout(appAbout)
                .setAppVersion(appVersion)
                .setLicense(licenseName, licenseUrl)
                .setShare(shareMsgText, shareIntentTitle)
                .setRateUs(appPlayStoreLink)
                .setInitiatedBy(poweredByIcon, poweredByTitle, poweredByName, poweredByAbout, poweredByLink)
                .setPoweredBy(initiatedByIcon, initiatedByTitle, initiatedByName, initiatedByAbout, initiatedByLink)
                .setSeeSourceCode(sourceCodeLink)
                .setThirdPartyLibrary(jsonResOfThirdPartyLibrary)
                .setCredits(jsonResOfCredits)
                .setContactUs(contactMail)
                .showAboutActivity();
    }

    public static void appShare(Activity activity){
        try {
            String appName = activity.getString(R.string.app_name) + " - Android App \n(அம்மாவுக்கான குழந்தை பராமரிப்பு)";
            String appPlayStoreLink = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
            String aboutApp = appName +  "\n\nபிரசவத்திற்குப் பிறகு தன்னையும் குழந்தையையும் கவனித்துக் கொள்ள தாய்க்கு இந்த மொபைல் செயலி உதவுகிறது. இதில் தாய் மற்றும் குழந்தைக்கான சுகாதார சேவை காணொளி மூலம் வழங்கப்படுகிறது.\n\nApp Link :\n" + appPlayStoreLink + "\n\nShare with your friends";
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, aboutApp);
            activity.startActivity(Intent.createChooser(intent, "Choose app to share..."));
        }catch (Exception e) {
            e.printStackTrace();
            ToastUtils.showWarning(activity, activity.getString(R.string.something_went_wrong));
        }
    }
}
