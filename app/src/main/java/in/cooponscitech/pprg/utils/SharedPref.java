package in.cooponscitech.pprg.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;


public class SharedPref {

    private static final String TAG = "SharedPref";
    // Shared pref file name
    private static final String PREF_NAME = "pprg_infant_care";
    private static final String IMAGE_HOST = "image_host";
    private static final String VIDEO_HOST = "video_host";
    private static final String LANGUAGE = "language";
    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private Editor editor;

    // Constructor
    @SuppressLint("CommitPrefEdits")
    public SharedPref(Context context) {
        // Shared pref mode
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public String getImageHost() {
        return TextUtils.isEmpty(pref.getString(IMAGE_HOST, "")) ? GeneralUtils.imgHost : pref.getString(IMAGE_HOST, GeneralUtils.imgHost);
    }

    public String getVideoHost() {
        return TextUtils.isEmpty(pref.getString(VIDEO_HOST, "")) ? GeneralUtils.videoHost : pref.getString(VIDEO_HOST, GeneralUtils.videoHost);
    }

    public String getLanguage() {
        return pref.getString(LANGUAGE, "தமிழ்");
    }

    public Boolean isTamil(){
        return getLanguage().equals("தமிழ்");
    }

    public void setLanguage(String language){
        editor.putString(LANGUAGE, language);
        editor.commit();
    }

    public void setHostData(String imgHost, String videoHost) {
        // mark user as logged in
        editor.putString(IMAGE_HOST, imgHost);
        editor.putString(VIDEO_HOST, videoHost);

        // commit changes
        editor.commit();
    }
}